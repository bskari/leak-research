import sys
import os
import struct

def RI(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
f = open(sys.argv[1], 'r')
SR = 0
GR = 0
LR = 0

VOBJ = [[],[]]
TOBJ = [[],[],[],[],[]]

LGT = []
VTX = []
TRI = []
triOff = 0
curLGT = []
curV = 0
curT = 0
uvScale = [1,1]
textureName = ""
for x in f:
        if(GR):
                if(x.find("gsSPLight(")>=0):
                        if(len(LGT)):
                                curLGT = LGT[int(x[x.find("[")+1:x.find("]")])]
                                
                elif(x.find("gsSPVertex(")>=0):
                        triOff = int(x[x.find("[")+1:x.find("]")])
                        if(curT==0):
                                name = x[x.find('&')+1:x.find('[')]
                                print(name)
                                for i,vtxN in enumerate(VOBJ[0]):
                                        print(vtxN)
                                        if(vtxN == name):
                                                
                                                curV = i
                                                TOBJ[2].append(curV)
                                                curT = 1
                elif(x.find("gsSP1Triangle(")>=0):
                        tr = x.replace("\tgsSP1Triangle(","").replace("),\n","").split(',')
                        tr.pop()
                        tf = []
                        for t in tr:
                                t = int(t) + triOff
                                if(len(curLGT)>0):
                                        #for a in range(3):
                                                #print curV
                                                #VOBJ[1][curV][t][6+a] = curLGT[a]
                                        for a in range(2):
                                                VOBJ[1][curV][t][4+a] = float(int(VOBJ[1][curV][t][4+a])>>6)
                                                VOBJ[1][curV][t][4+a] /= uvScale[a]
                                tf.append(t)
                        TRI.append(tf)
                elif(x.find("LoadTextureImage")>=0):
                        uvScale = list(map(float,x[x.find("(")+1:x.find(")")].split(',')[3:5]))
                        uvScale[0] -= float(1)
                        uvScale[1] -= float(1)
                        textureName = x[x.find("(")+1:x.find(")")].split(',')[0]
                #else: print(x)
        if(SR):
                #print x
                if(x.find('};') == -1):
                        preFixed = x[x.find('{')+1:x.find('}')].split(',')
                        fixedUp = []
                        for fixorino in preFixed:
                                fixr = 0
                                if(fixorino.find("-")>0 and len(fixorino.split("-"))>1 and RI(fixorino.split("-")[0])):
                                        print(fixorino.split("-"))
                                        fixr = float(fixorino.split("-")[0])-float(fixorino.split("-")[1])
                                elif(fixorino.find("/")>0):
                                        fixr = float(fixorino.split("/")[0])/float(fixorino.split("/")[1])
                                elif(fixorino.find("+")>0):
                                        fixr = float(fixorino.split("+")[0])+float(fixorino.split("+")[1])
                                elif(fixorino.find("*")>0):
                                        fixr = float(fixorino.split("*")[0])*float(fixorino.split("*")[1])
                                else:
                                        fixr = float(fixorino)
                                fixedUp.append(fixr)
                                
                        VTX.append(fixedUp)
        if(LR):
                #print x[x.find('(')+1:x.find(')')].split(',')
                LGT.append(x[x.find('(')+1:x.find(')')].split(','))
                
        if(x.find('Gfx ') >= 0 ):
                GR = 1
                TOBJ[0].append(x[x.find("Gfx ")+4:x.find("[")])
                print(x[x.find("Gfx ")+4:x.find("[")])
        if(x.find('Lights1') >= 0 ):
                #print x
                LR = 1
        if(x.find('static Vtx') >= 0):
                SR = 1
                VOBJ[0].append(x[x.find("Vtx ")+4:x.find("[")])
        if(x.find('};') == 0):
                if(SR): 
                        VOBJ[1].append(VTX)
                        VTX = []
                if(GR):
                        TOBJ[1].append(TRI)
                        TOBJ[3].append(curLGT)
                        TOBJ[4].append(textureName)
                        textureName = ""
                        TRI = []
                SR = 0
                curT = 0
                GR = 0
                LR = 0
                #print "end"
VOOF = 0
mtl = open(sys.argv[1] + ".mtl", 'w')
for e in range(len(TOBJ[3])):
        mtl.write("newmtl %s\n" % TOBJ[0][e])
        mtl.write("Kd %f %f %f\n" % (float(TOBJ[3][e][0])/255,float(TOBJ[3][e][1])/255,float(TOBJ[3][e][2])/255))
        mtl.write("illum 1\n")
        if(len(TOBJ[4][e])>0):
                mtl.write("map_Kd %s"%TOBJ[4][e])
        mtl.write("\n")

o = open(sys.argv[1] + ".obj", 'w')
o.write("mtllib " +sys.argv[1] + ".mtl\n")
for e in range(len(TOBJ[2])):
        #print(TOBJ[2][e])
        VTX = VOBJ[1][TOBJ[2][e]]
        TRI = TOBJ[1][e]
        
        o.write("o %s\n"%TOBJ[0][e])    
        for v in VTX:
                o.write("v %s %s %s\n" % (v[0],v[1],v[2]))
        for v in VTX:
                o.write("vt %.6f %.6f\n" % (v[4],v[5]))
        for v in VTX:
                o.write("vn %.6f %.6f %.6f\n" % (float(float(v[5])/255),float(float(v[6])/255),float(float(v[7])/255)))
       
        maxTri = int(len(VTX))
        o.write("usemtl %s\n" % TOBJ[0][e])
        o.write("s 1\n")
        for p in TRI:
                o.write("f ")
                for pp in p:
                        for asd in range(3):
                                o.write("%i" % (int(pp)+1+VOOF))
                                if(asd<2):
                                        o.write("/")
                        o.write(" ")
                o.write("\n")
        VOOF += len(VTX)

/*
Project: texarray2bmp
Purpose: Convert an array of pixel data to a bitmap file
Author(s): Regan "cuckydev" Green
*/

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <getopt.h>

//Texture format structs
typedef enum
{
	TF_RGBA5551,
	TF_MAX,
	TF_NULL = TF_MAX,
} TexFormat;

typedef struct
{
	uint32_t bits, shift, loss, mask;
} TexFormatChannel;

typedef struct
{
	int bytes_per_pixel, bits_per_pixel;
	TexFormatChannel r, g, b, a;
} TexFormatPixel;

//Texture format channel and pixels
uint8_t TFC_Get(const TexFormatChannel *channel, uint32_t value)
{
	uint8_t p1 = (value & channel->mask) >> channel->shift;
	uint8_t p2 = ((1 << channel->loss)-1) * p1 / ((1 << channel->bits)-1);
	return (p1 << channel->loss) | p2;
}

void TFP_GetRGBA(const TexFormatPixel *pixel, uint32_t value, uint8_t *r, uint8_t *g, uint8_t *b, uint8_t *a)
{
	if (r != NULL)
		*r = TFC_Get(&pixel->r, value);
	if (g != NULL)
		*g = TFC_Get(&pixel->g, value);
	if (b != NULL)
		*b = TFC_Get(&pixel->b, value);
	if (a != NULL)
		*a = TFC_Get(&pixel->a, value);
}

//Texture formats
typedef struct
{
	TexFormat format;
	TexFormatPixel pixel;
	const char *name;
} TexFormatSpec;

TexFormatSpec tex_formats[TF_MAX] = {
	{TF_RGBA5551, {2, 16, {5,10,3,0xF800}, {5,6,3,0x07C0}, {5,1,3,0x003E}, {1,0,7,0x0001}}, "RGBA5551"},
};

//Texture format interface
const TexFormatSpec *TexFormatSpecFromFormat(TexFormat format)
{
	for (int i = 0; i < TF_MAX; i++)
		if (tex_formats[i].format == format)
			return &tex_formats[i];
	return NULL;
}

const TexFormatSpec *TexFormatSpecFromName(const char *name)
{
	for (int i = 0; i < TF_MAX; i++)
		if (!strcmp(tex_formats[i].name, name))
			return &tex_formats[i];
	return NULL;
}

TexFormat TexFormatFromName(const char *name)
{
	for (int i = 0; i < TF_MAX; i++)
		if (!strcmp(tex_formats[i].name, name))
			return tex_formats[i].format;
	return TF_NULL;
}

const char *TexFormatToName(TexFormat format)
{
	for (int i = 0; i < TF_MAX; i++)
		if (tex_formats[i].format == format)
			return tex_formats[i].name;
	return NULL;
}

//Endian independant file writing
void fwrite_be16(FILE *fp, uint16_t value)
{
	fputc(value >> 8, fp);
	fputc(value >> 0, fp);
}

void fwrite_be32(FILE *fp, uint32_t value)
{
	fputc(value >> 24, fp);
	fputc(value >> 16, fp);
	fputc(value >>  8, fp);
	fputc(value >>  0, fp);
}

void fwrite_be64(FILE *fp, uint64_t value)
{
	fputc(value >> 56, fp);
	fputc(value >> 48, fp);
	fputc(value >> 40, fp);
	fputc(value >> 32, fp);
	fputc(value >> 24, fp);
	fputc(value >> 16, fp);
	fputc(value >>  8, fp);
	fputc(value >>  0, fp);
}

void fwrite_le16(FILE *fp, uint16_t value)
{
	fputc(value >> 0, fp);
	fputc(value >> 8, fp);
}

void fwrite_le32(FILE *fp, uint32_t value)
{
	fputc(value >>  0, fp);
	fputc(value >>  8, fp);
	fputc(value >> 16, fp);
	fputc(value >> 24, fp);
}

void fwrite_le64(FILE *fp, uint64_t value)
{
	fputc(value >>  0, fp);
	fputc(value >>  8, fp);
	fputc(value >> 16, fp);
	fputc(value >> 24, fp);
	fputc(value >> 32, fp);
	fputc(value >> 40, fp);
	fputc(value >> 48, fp);
	fputc(value >> 56, fp);
}

//Parse file template
#define T_ParseFile(p,T) \
void ParseFile##p(char *input, void *data) \
{ \
	T *datap = (T*)data; \
	char *piece = NULL; \
	while ((piece = strtok((piece != NULL) ? NULL : input, " ,\r\n\t")) != NULL) \
	{ \
		/*Push data to buffer*/ \
		*datap++ = (T)strtoull(piece, NULL, 0); \
	} \
}

T_ParseFile(1,uint8_t)  //defines ParseFile1
T_ParseFile(2,uint16_t) //defines ParseFile2
T_ParseFile(4,uint32_t) //defines ParseFile4
//T_ParseFile(8,uint64_t) //defines ParseFile8

//Read input file
int ReadInput(const char *in, const TexFormatSpec *tex_format, void *data)
{
	//Read input file as raw hex
	FILE *in_fp = fopen(in, "r");
	if (in_fp == NULL)
	{
		puts("Failed to open input file");
		return -1;
	}
	
	//Read entire file into buffer (would be simpler if C had fopen at end of file)
	fseek(in_fp, 0, SEEK_END);
	size_t size = ftell(in_fp);
	fseek(in_fp, 0, SEEK_SET);
	
	char *input = malloc(size + 1);
	size_t read_bytes = fread(input, 1, size, in_fp);
	fclose(in_fp);
	
	if (read_bytes != size)
	{
		puts("Reading failed, somehow");
		return -1;
	}
	
	//Set input terminator key and close input file
	input[size] = 0;
	
	//Parse file
	switch (tex_format->pixel.bytes_per_pixel)
	{
		case 1:
			ParseFile1(input, data);
			break;
		case 2:
			ParseFile2(input, data);
			break;
		case 4:
			ParseFile4(input, data);
			break;
		/*
		case 8:
			ParseFile8(input, data);
			break;
		*/
	}
	return 0;
}

//Write pixels template
#define T_WritePixels(p,T) \
void WritePixels##p(FILE *out_fp, void *data, int width, int height, const TexFormatPixel *tex_pixel) \
{ \
	T *datap = (T*)data; \
	for (int i = 0; i < width * height; i++) \
	{ \
		uint8_t r, g, b, a; \
		TFP_GetRGBA(tex_pixel, *datap++, &r, &g, &b, &a); \
		fwrite_le32(out_fp, ((uint32_t)r << 24) | ((uint32_t)g << 16) | ((uint32_t)b << 8) | ((uint32_t)a << 0)); \
	} \
}

T_WritePixels(1,uint8_t)  //defines WritePixels1
T_WritePixels(2,uint16_t) //defines WritePixels2
T_WritePixels(4,uint32_t) //defines WritePixels4
//T_WritePixels(8,uint64_t) //defines WritePixels8

//Write output file
int WriteOutput(const char *out, void *data, int width, int height, const TexFormatSpec *tex_format)
{
	//Open output file
	FILE *out_fp = fopen(out, "wb");
	if (out_fp == NULL)
	{
		puts("Failed to open output file");
		return -1;
	}
	
	//Write header
	fwrite_be16(out_fp, '0x424D'); //"BM"
	fwrite_le32(out_fp, 0x7A + (width*height*4)); //size TODO
	fwrite_le16(out_fp, 0); //reserved 1
	fwrite_le16(out_fp, 0); //reserved 2
	fwrite_le32(out_fp, 0x7A); //position
	
	//Write info header
	fwrite_le32(out_fp, 0x6C); //header size
	fwrite_le32(out_fp, width); //width
	fwrite_le32(out_fp, -height); //height
	fwrite_le16(out_fp, 1); //planes
	fwrite_le16(out_fp, 32); //bpp
	fwrite_le32(out_fp, 3); //compression - BITFIELDS
	fwrite_le32(out_fp, width * height * 4); //size of bitmap data
	fwrite_le32(out_fp, 2835); //physical width
	fwrite_le32(out_fp, 2835); //physical height
	fwrite_le32(out_fp, 0); //number of colours
	fwrite_le32(out_fp, 0); //important colours
	
	//Bitfields
	fwrite_le32(out_fp, 0xFF000000);
	fwrite_le32(out_fp, 0x00FF0000);
	fwrite_le32(out_fp, 0x0000FF00);
	fwrite_le32(out_fp, 0x000000FF);
	fwrite_le32(out_fp, 0x576E6957); //"Win " (yes it's supposed to be little-endian)
	for (int i = 0; i < 0x24/4+3; i++)
		fwrite_le32(out_fp, 0x576E6957); //"Win "
	
	//Write pixel data
	switch (tex_format->pixel.bytes_per_pixel)
	{
		case 1:
			WritePixels1(out_fp, data, width, height, &tex_format->pixel);
			break;
		case 2:
			WritePixels2(out_fp, data, width, height, &tex_format->pixel);
			break;
		case 4:
			WritePixels4(out_fp, data, width, height, &tex_format->pixel);
			break;
		/*
		case 8:
			WritePixels8(out_fp, data, width, height, &tex_format->pixel);
			break;
		*/
	}
	
	//Close output file
	fclose(out_fp);
	return 0;
}

//Entry point
int main(int argc, char *argv[])
{
	//Arguments
	const char *in = NULL, *out = NULL;
	char *def_out = NULL;
	int width = 0, height = 0;
	const TexFormatSpec *tex_format = NULL;
	
	if (argc > 1 && (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")))
	{
		printf("\
texarray2bmp usage:\n\
  -i input path OR --in input path\n\
  -o output path OR --out output ath\n\
  -w width OR --width width\n\
  -h height OR --height height\n\
  -f format OR --format format\n\
");
		return 0;
	}
	
	//Parse arguments
	const char *option_key = "i:o:w:h:b:f:";
	static const struct option options[] = {
		{"in", required_argument, NULL, 0},
		{"out", required_argument, NULL, 0},
		{"width", required_argument, NULL, 0},
		{"height", required_argument, NULL, 0},
		{"format", required_argument, NULL, 0},
		{0, 0, NULL, 0}
	};
	
	int option_index;
	int c;
	while ((c = getopt_long(argc, argv, option_key, options, &option_index)) != -1)
	{
		//If a long argument was used, convert it to short index
		if (c == 0)
		{
			const char *v = option_key;
			for (int i = 0; i < option_index; i++)
			{
				v++;
				if (*v == ':')
					v++;
			}
			c = *v;
		}
		
		//Handle argument
		switch (c)
		{
			case 'i':
				in = optarg;
				break;
			case 'o':
				out = optarg;
				break;
			case 'w':
				width = atoi(optarg);
				break;
			case 'h':
				height = atoi(optarg);
				break;
			case 'f':
				tex_format = TexFormatSpecFromName(optarg);
				break;
			default:
				return -1;
		}
	}
	
	//Check arguments
	if (in == NULL)
	{
		puts("Input file was not given. (argument: -i path OR --in path)");
		return -1;
	}
	
	if (width <= 0 || height <= 0)
	{
		puts("Width or height are invalid or weren't given. Must be an integer and > 0. (arguments: -w/-h int OR --width/--height int)");
		return -1;
	}
	
	if (tex_format == NULL)
	{
		printf("Texture format was either invalid or wasn't given. ");
		for (int i = 0; i < TF_MAX-1; i++)
			printf("%s, ", tex_formats[i].name);
		if (TF_MAX > 1)
			printf("or %s expected. (argument: -f format OR --format format)\n", tex_formats[TF_MAX-1].name);
		else if (TF_MAX > 0)
			printf("%s expected. (argument: -f format OR --format format)\n", tex_formats[TF_MAX-1].name);
		return -1;
	}
	
	if (out == NULL)
	{
		//Allocate new output path
		def_out = malloc(strlen(in) + 4 + 1);
		if (def_out == NULL)
		{
			printf("Failed to malloc default output path.\n");
			return -1;
		}
		
		//Get point where extension starts in input path
		int in_point = strlen(in) - 1;
		while (in_point >= 0 && in[in_point] != '.')
			in_point--;
		if (in_point < 0)
			in_point = strlen(in);
		
		//Copy input file up to extension and append .bmp
		memcpy(def_out, in, in_point);
		strcat(def_out, ".bmp");
		
		//Use and warn user of new output path
		out = def_out;
		printf("Output file was not given. Using %s instead. (argument: -o path OR --out path)\n", def_out);
	}
	
	//Print usage
	printf("in: %s out: %s width: %d height: %d format: %s\n", in, out, width, height, tex_format->name);
	
	//Allocate data buffer
	void *data = malloc((width * height * tex_format->pixel.bits_per_pixel + 7) / 8);
	if (data == NULL)
	{
		puts("Failed to allocate data buffer");
		free(def_out);
		return -1;
	}
	
	//Read input and write output files
	if (ReadInput(in, tex_format, data) ||
		WriteOutput(out, data, width, height, tex_format))
	{
		free(data);
		free(def_out);
		return -1;
	}
	
	//Free buffers
	free(data);
	free(def_out);
	return 0;
}

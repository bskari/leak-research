# List of tools + credits

smb123w64gb - sou2obj + sou2ply - convert Mario 64 model formats  

ndiddy - as65c syntax - notes on AS65C syntax  

lugiblood - sf.xml - running builds of Starfox 2 without having to change the header every time on bsnes-plus  

prism - agbconvengv3.ips - English patch for AgbConv 

cuckydev - texarray - Convert an array of pixel data to a bitmap file  

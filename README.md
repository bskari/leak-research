# Leak research

The hub for the whole research project :)

In this house, we follow the TCRF guidelines for leaks: https://tcrf.net/Help:Contents/Rules_%26_Guidelines/Leaks

This means: no direct dumps from the leaks, no links to the leaks, no ROMs, etc. Just documentation and research.

Although, in the name of "please don't go get yourself a virus", here are SHA256 hashes of the main archives:

| Filename | SHA256 | Contents |
| --- | --- | --- |
| 20100713cvs_backup.tar.7z | `6c5bf8cdbd4773261416fbbe603a62f05f241709d69397f4101100e1c4fa86e6` | Pokémon D/P source repo, among other things |
| bbgames.7z | `632f1ccf78600ac8d1f8690caf24be6cf7ec9fd9ae0dc74e892fa5d8921115f0` | iQue N64 leaks |
| fuckbees.zip | `845c3cdc8993076e2968bb55224f872ace98e4b24260ee4a1d338357e40faa5e` | Various Wii documents | `845c3cdc8993076e2968bb55224f872ace98e4b24260ee4a1d338357e40faa5e` |
| netcard.7z | `0b042752602678eccd8a617dd1818b01eba860e03226c6eb97ed31ce6c195e13` | "Netcard" project, GBA/Wii documentation, more iQue stuff |
| other.7z | `e49a80c79265caa31224223bf2d852c8fa69ef144939993c23070e833ae07e46` | NEWS tape archives, SNES-era sources, various prototypes |
| other.zip | `f46e7213a708596c86a22c580511dd4e0e88e4aa099c74f1e322b85422e76f29` | 3DS kiosk dump |